package com.joseestudillo.mapreduce;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Test;

import com.joseestudillo.hadoop.mapreduce.ReducerImpl;

/**
 * 
 * @author Jose Estudillo
 *
 */
public class ReducerImplTest {

	@Test
	public void returnsMaximumIntegerInValues() throws IOException, InterruptedException {
		Pair<Text, List<IntWritable>> inputPair = new Pair<Text, List<IntWritable>>(new Text("test"), Arrays.asList(new IntWritable(1), new IntWritable(2),
				new IntWritable(3)));
		Pair<Text, IntWritable> outputPair = new Pair<Text, IntWritable>(new Text("test"), new IntWritable(6));

		new ReduceDriver<Text, IntWritable, Text, IntWritable>()
				.withReducer(new ReducerImpl())
				.withInput(inputPair)
				.withOutput(outputPair)
				.runTest();
	}
}
