package com.joseestudillo.mapreduce;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Test;

import com.joseestudillo.hadoop.mapreduce.MapperImpl;

/**
 * 
 * @author Jose Estudillo
 *
 */
public class MapperImplTest {

	@Test
	public void processesValidRecord() throws IOException, InterruptedException {
		Text value = new Text("Text");
		new MapDriver<LongWritable, Text, Text, IntWritable>()
				.withMapper(new MapperImpl())
				.withInput(new Pair<LongWritable, Text>(new LongWritable(), value))
				.withOutput(new Text("Text"), new IntWritable(1))
				.runTest();
	}

	@Test
	public void customCounterTest() throws IOException {
		Counters counters = new Counters();
		new MapDriver<LongWritable, Text, Text, IntWritable>()
				.withMapper(new MapperImpl())
				.withInput(new Pair<LongWritable, Text>(new LongWritable(0), new Text("Text")))
				.withCounters(counters)
				.withOutput(new Text("Text"), new IntWritable(1))
				.runTest();
		Counter c = counters.findCounter(MapperImpl.CounterNames.MAP_CALLS);
		assertThat(c.getValue(), is(1L));
	}
}
