package com.joseestudillo.hadoop.jobrunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.io.PrintWriter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;
import org.junit.Test;

import com.google.common.io.Files;
import com.joseestudillo.hadoop.util.HadoopUtils;

/**
 * 
 * @author Jose Estudillo
 *
 */
public class ToolJobRunnerTest {

	private static final Logger log = Logger.getLogger(ToolJobRunnerTest.class);

	@Test
	public void testToolJobRunner() throws Exception {
		Configuration conf = HadoopUtils.buildLocalConfiguration();
		String inputPath = String.valueOf(this.getClass().getClassLoader().getResource("input"));
		String outputPath = String.valueOf(Files.createTempDir());
		Path input = new Path(inputPath);
		Path output = new Path(outputPath);

		log.info(String.format("Testing {input: %s, output: %s}", inputPath, outputPath));

		FileSystem fs = FileSystem.getLocal(conf);
		fs.delete(output, true); // delete old output
		ToolJobRunner driver = new ToolJobRunner();
		driver.setConf(conf);
		int exitCode = driver.run(new String[] { input.toString(), output.toString() });
		assertThat(exitCode, is(0));
		//checkOutput(conf, output);
	}

	@Test
	public void testCompleteToolJobRunner() throws Exception {
		Configuration conf = HadoopUtils.buildLocalConfiguration();
		String inputPath = String.valueOf(this.getClass().getClassLoader().getResource("input"));
		String outputPath = String.valueOf(Files.createTempDir());
		Path input = new Path(inputPath);
		Path output = new Path(outputPath);

		log.info(String.format("Testing {input: %s, output: %s}", inputPath, outputPath));

		FileSystem fs = FileSystem.getLocal(conf);
		fs.delete(output, true); // delete old output
		CompleteToolJobRunner driver = new CompleteToolJobRunner();
		driver.setConf(conf);
		int exitCode = driver.run(new String[] { input.toString(), output.toString() });
		assertThat(exitCode, is(0));
		//checkOutput(conf, output);
		HadoopUtils.printOutputFolder(new File(outputPath), new PrintWriter(System.out));
	}
}
