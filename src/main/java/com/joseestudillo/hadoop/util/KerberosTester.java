package com.joseestudillo.hadoop.util;

import java.io.PrintWriter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

/**
 * Tool that shows the tree structure of a given folder in HDFS using kerberos
 * 
 * @author Jose Estudillo
 *
 */
public class KerberosTester extends Configured {

	private static final String ROOT_DIR = "/";

	private static final Logger log = Logger.getLogger(KerberosTester.class);

	private static final String NO_CONFIG_DIR = "";

	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.err.printf("Usage: %s <principal> <keytab path> [hadoop configuration]\n", KerberosTester.class.getSimpleName());
			ToolRunner.printGenericCommandUsage(System.err);
			System.exit(-1);
		}

		String principal = args[0];
		String keytabPath = args[1];
		String confDirPath = (args.length > 2) ? args[2] : NO_CONFIG_DIR;
		String path = (args.length > 3) ? args[3] : ROOT_DIR;

		Configuration configuration = HadoopUtils.loadConfig(confDirPath, log);

		HadoopUtils.showConfig(configuration, System.out);

		HadoopUtils.setKerberosConfig(configuration, principal, keytabPath);

		HadoopUtils.tree(configuration, new Path(path), new PrintWriter(System.out));
	}
}
