package com.joseestudillo.hadoop.util;

import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

/**
 * Tool to show the hadoop configuration for a given installation
 * 
 * if the path is not specified, it will assume the env var "HADOOP_INSTALL" declared and pointing to the hadoop installation to pick the xml files
 * 
 * @author Jose Estudillo
 *
 */
public class HadoopConfigPrinter extends Configured implements Tool {

	private static final Logger log = Logger.getLogger(HadoopConfigPrinter.class);

	@Override
	public int run(String[] args) throws Exception {
		if (args.length > 0) {
			log.info(String.format("Loading configuration from %s", args[0]));
			this.setConf(HadoopUtils.loadConfig(args[0], log));
		}
		printConfiguration(this.getConf());
		return 0;
	}

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new HadoopConfigPrinter(), args);
		System.exit(exitCode);
	}

	public static void printConfiguration(Configuration configuration) {
		for (Entry<String, String> entry : configuration) {
			System.out.printf("%s=%s\n", entry.getKey(), entry.getValue());
		}
	}
}