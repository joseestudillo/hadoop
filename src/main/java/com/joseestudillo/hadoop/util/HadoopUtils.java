package com.joseestudillo.hadoop.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.CommonConfigurationKeysPublic;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.log4j.Logger;

/**
 * Utility class for Hadoop
 * 
 * @author Jose Estudillo
 *
 */
public class HadoopUtils {

	public static final String LOCAL_CONFIG = "local";
	private static final String CRC_EXTENSION_REGEX = ".*\\.[cC][rR][cC]$";
	public static final String DEFAUL_INPUT_PATH = "/tmp/input";
	public static final String DEFAUL_OUTPUT_PATH_PTTR = "/tmp/%s";

	/**
	 * Prints the content of a local file (non-hadoop)
	 * 
	 * @param file
	 * @param pw
	 * @throws IOException
	 */
	public static void printFileContent(File file, PrintWriter pw) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			pw.println(line);
		}
		br.close();
	}

	/**
	 * Prints the content of a directory and the content of its files
	 * 
	 * @param outputFolder
	 * @param pw
	 * @throws IOException
	 */
	public static void printOutputFolder(File outputFolder, PrintWriter pw) throws IOException {
		for (File file : outputFolder.listFiles()) {
			if (file.isFile() && !file.getName().matches(CRC_EXTENSION_REGEX)) {
				pw.format("\n->%s:\n", file.getAbsolutePath());
				printFileContent(file, pw);
			}
		}
		pw.flush();
	}

	/**
	 * Prints the root directory tree structure similar to the command tree
	 * 
	 * @param configuration
	 * @param pw
	 * @throws IOException
	 */
	public static void tree(Configuration configuration, PrintWriter pw) throws IOException {
		tree(configuration, new Path("/"), pw);
	}

	/**
	 * Prints a given directory tree structure similar to the command tree
	 * 
	 * @param configuration
	 * @param path
	 * @param pw
	 * @throws IOException
	 */
	public static void tree(Configuration configuration, Path path, PrintWriter pw) throws IOException {
		FileSystem fs = FileSystem.get(configuration);
		pw.format("tree %s", path);
		showFileSystemEntry(fs, fs.getFileStatus(path), pw, "");
		pw.flush();
	}

	private static void showFileSystemEntry(FileSystem fileSystem, FileStatus fileStatus, PrintWriter output, String margin) throws FileNotFoundException,
			IOException {

		if (fileStatus.isDirectory()) {
			output.format("%s%s\n", margin, String.valueOf(getName(fileStatus)));
			margin += " ";
			for (FileStatus tmpFileStatus : fileSystem.listStatus(fileStatus.getPath())) {
				showFileSystemEntry(fileSystem, tmpFileStatus, output, margin);
			}
		} else {
			output.format("%s%s\n", margin, String.valueOf(getName(fileStatus)));
		}
	}

	private static String getName(FileStatus fileStatus) {
		String name = fileStatus.getPath().getName();
		if (name.length() == 0) {
			return name = "/";
		}
		return name;
	}

	/**
	 * Builds a <code>Configuration</code> object with the information contained in a properties file
	 * 
	 * @param properties
	 * @return Hadoop's configuration set up with the properties contained in the given file
	 */
	public static Configuration buildConfiguration(Properties properties) {
		Configuration configuration = new Configuration();
		for (Entry<Object, Object> entry : properties.entrySet()) {
			configuration.set(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
		}

		return configuration;
	}

	/**
	 * Returns a configuration to run a job locally. Specially useful for testing purposes.
	 * 
	 * @return A Hadoop's local configuration
	 */
	public static Configuration buildLocalConfiguration() {
		Configuration configuration = new Configuration();
		configuration.set("fs.defaultFS", "file:///");
		configuration.set("mapreduce.jobtracker.address", "local");
		return configuration;
	}

	/** hadoop startdan configuration names */
	private static final List<String> HADOOP_CONFIG_FILENAMES = Arrays.asList("core-site.xml", "hdfs-site.xml", "mapred-site.xml",
			"yarn-site.xml");

	/**
	 * Loads the hadoop configuration from a property file contained in the classpath
	 * 
	 * @param properityFilename
	 * @return
	 * @throws IOException
	 */
	public static Configuration loadConfigFromProperties(String properityFilename) throws IOException {
		Properties configurationProperties = new Properties();
		configurationProperties.load(HadoopUtils.class.getResourceAsStream(properityFilename));
		Configuration configuration = HadoopUtils.buildConfiguration(configurationProperties);
		return configuration;
	}

	public static Configuration loadConfigFromHadoopXMLConfigs() throws FileNotFoundException {
		return loadConfigFromHadoopXMLConfigDir("/config");
	}

	private static Configuration loadConfigFromHadoopXMLConfigs(List<String> configFiles) throws FileNotFoundException {
		Configuration configuration = new Configuration();
		for (String hadoopConfigFilePath : configFiles) {
			InputStream in = new FileInputStream(new File(hadoopConfigFilePath));
			configuration.addResource(in);
		}
		return configuration;
	}

	public static Configuration loadConfigFromHadoopXMLConfigDir(String configDirPath) throws FileNotFoundException {
		return loadConfigFromHadoopXMLConfigs(getDefaultConfigFilePathsFromDir(configDirPath));
	}

	private static List<String> getDefaultConfigFilePathsFromDir(String baseDir) {
		List<String> filepaths = new ArrayList<String>();
		for (String filename : HADOOP_CONFIG_FILENAMES) {
			filepaths.add(String.format("%s/%s", baseDir, filename));
		}
		return Collections.unmodifiableList(filepaths);
	}

	//TODO add checks for the slashes in the paths
	public static boolean put(Configuration hadoopConf, File file, String hadoopOutputDirPath) throws IOException {
		Path path = new Path(String.format("%s/%s", hadoopOutputDirPath, file.getName()));
		return put(hadoopConf, file, path);
	}

	public static boolean put(Configuration hadoopConf, File file, Path destFilePath) throws IOException {
		FileSystem fs = FileSystem.get(hadoopConf);
		return FileUtil.copy(file, fs, destFilePath, false, hadoopConf);
	}

	public static StringBuilder cat(Configuration hadoopConf, Path filePath) throws IOException {
		return cat(hadoopConf, filePath, new StringBuilder());
	}

	public static StringBuilder cat(FileSystem fs, Path filePath) throws IOException {
		return cat(fs, filePath, new StringBuilder());
	}

	public static StringBuilder cat(Configuration hadoopConf, Path filePath, StringBuilder strb) throws IOException {
		FileSystem fs = FileSystem.get(hadoopConf);
		return cat(fs, filePath, strb);
	}

	public static StringBuilder cat(FileSystem fs, Path filePath, StringBuilder strb) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(filePath).getWrappedStream()));
		String line;
		while ((line = br.readLine()) != null) {
			strb.append(line);
			strb.append("\n");
		}
		return strb;
	}

	public static void showConfig(Configuration config, OutputStream os) {
		PrintWriter pw = new PrintWriter(os);
		for (Iterator<Entry<String, String>> iter = config.iterator(); iter.hasNext();) {
			Entry<String, String> entry = iter.next();
			pw.println(String.format("%s -> %s", entry.getKey(), entry.getValue()));
		}
		pw.flush();
	}

	public static Configuration loadConfigFromWorkspace(String configPath) throws FileNotFoundException {
		Configuration configuration = new Configuration();
		List<String> configFiles = getDefaultConfigFilePathsFromDir(configPath);
		for (String hadoopConfigFilePath : configFiles) {
			configuration.addResource(HadoopUtils.class.getResourceAsStream(hadoopConfigFilePath));
		}
		return configuration;
	}

	public static Configuration loadConfigFromDirectory(File configDir) throws FileNotFoundException {
		Configuration configuration = new Configuration();
		if (configDir.isDirectory()) {
			List<String> configFiles = getDefaultConfigFilePathsFromDir(configDir.getPath());
			for (String hadoopConfigFilePath : configFiles) {
				configuration.addResource(new FileInputStream(new File(hadoopConfigFilePath)));
			}
		}
		return configuration;
	}

	public static String[] setUpDummyInputOutput(FileSystem fs, Logger log) throws IOException {
		String inputPath = DEFAUL_INPUT_PATH;
		Path hadoopInputPath = new Path(inputPath);
		if (!fs.exists(hadoopInputPath)) {
			fs.mkdirs(hadoopInputPath);
			log.info(String.format("Folder %s created in hdfs", hadoopInputPath));
		}

		Path hadoopInputFilePath = new Path(inputPath + "file.txt");
		if (!fs.exists(hadoopInputFilePath)) {
			FSDataOutputStream dos = fs.create(hadoopInputFilePath);
			for (String str : Arrays.asList("1", "2", "3", "a", "b", "c")) {
				dos.writeChars(str + "\n");
			}
			dos.close();
			log.info(String.format("Dummy input file %s generated in hdfs", hadoopInputFilePath));
		}

		String outputPath = String.format(DEFAUL_OUTPUT_PATH_PTTR, System.currentTimeMillis());
		return new String[] { inputPath, outputPath };
	}

	public static Configuration loadConfig(String configPath, Logger log) throws FileNotFoundException {
		Configuration configuration;
		if (configPath.length() > 0) {
			if (configPath.equals(HadoopUtils.LOCAL_CONFIG)) {
				log.info("Running in local mode");
				configuration = HadoopUtils.buildLocalConfiguration();
			} else {
				log.info(String.format("Loading configuration from %s", configPath));
				configuration = HadoopUtils.loadConfigFromDirectory(new File(configPath));//HadoopUtils.loadConfigFromWorkspace(configPath);
			}
		} else {
			configuration = new Configuration(); //this allow to execute using the hadoop command, so the config will be provided automatically
		}
		return configuration;
	}

	public static StringBuilder catJobResults(FileSystem fs, Path outputDir) throws FileNotFoundException, IOException {
		StringBuilder strb = new StringBuilder();
		for (FileStatus fStatus : fs.listStatus(outputDir)) {
			if (getName(fStatus).startsWith("part")) {
				cat(fs, fStatus.getPath(), strb);
			}
		}
		return strb;
	}

	public static void setKerberosConfig(Configuration conf, String principal, String keytabPath) throws IOException {
		conf.set(CommonConfigurationKeysPublic.HADOOP_SECURITY_AUTHENTICATION, "Kerberos");
		UserGroupInformation.setConfiguration(conf);
		UserGroupInformation.loginUserFromKeytab(principal, keytabPath);
	}
}
