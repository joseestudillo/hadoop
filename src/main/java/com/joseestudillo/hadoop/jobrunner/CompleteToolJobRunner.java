package com.joseestudillo.hadoop.jobrunner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import com.joseestudillo.hadoop.comparator.GroupComparator;
import com.joseestudillo.hadoop.comparator.KeyComparator;
import com.joseestudillo.hadoop.mapreduce.CombinerImpl;
import com.joseestudillo.hadoop.mapreduce.MapperImpl;
import com.joseestudillo.hadoop.mapreduce.PartitionerImpl;
import com.joseestudillo.hadoop.mapreduce.ReducerImpl;
import com.joseestudillo.hadoop.partitioner.TextIntPairPartitioner;
import com.joseestudillo.hadoop.util.HadoopUtils;

/**
 * An example of JobRunner including all the steps and configurations
 * 
 * @author Jose Estudillo
 *
 */
public class CompleteToolJobRunner extends Configured implements Tool {

	private static final Logger log = Logger.getLogger(CompleteToolJobRunner.class);

	@Override
	public int run(String[] args) throws Exception {
		if (args.length != 2) {
			System.err.printf("Usage: %s [generic options] <input> <output>\n", this.getClass().getSimpleName());
			ToolRunner.printGenericCommandUsage(System.err);
			return -1;
		}
		Configuration configuration = this.getConf();
		String username = configuration.get("hadoop.job.ugi");
		System.setProperty("HADOOP_USER_NAME", username != null ? username : "");
		Job job = Job.getInstance(configuration, "Word Count");
		job.setJarByClass(this.getClass());

		String inputPath = args[0];
		String outputPath = args[1];

		FileInputFormat.addInputPath(job, new Path(inputPath));
		FileOutputFormat.setOutputPath(job, new Path(outputPath));

		job.setMapperClass(MapperImpl.class);
		//combiner added for clarity, usually the reducer will be enough
		job.setCombinerClass(CombinerImpl.class);
		job.setReducerClass(ReducerImpl.class);
		job.setPartitionerClass(PartitionerImpl.class);

		job.setInputFormatClass(TextInputFormat.class);// set automatically when the input is set
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		//set the comparators
		//comparator used during the mapping
		job.setSortComparatorClass(KeyComparator.class);
		//comparator used during the reducing
		job.setGroupingComparatorClass(GroupComparator.class);

		//partitioner
		job.setPartitionerClass(TextIntPairPartitioner.class); //this is only used with the number of reduce task is > 1
		job.setNumReduceTasks(2); //this will generate as many part-XXXX files as # of reduce tasks specified

		int exitCode = job.waitForCompletion(true) ? 0 : 1;

		log.info(String.format("Job: %s done. Input Path: %s, OutputPath: %s", job.getJobID(), inputPath, outputPath));
		StringBuilder output = HadoopUtils.catJobResults(FileSystem.get(configuration), new Path(outputPath));
		log.info(String.format("Results: \n%s", output.toString()));
		return exitCode;
	}

	public static void main(String[] args) throws Exception {
		local(args);
	}

	public static void main(Configuration configuration, String[] args) throws Exception {
		CompleteToolJobRunner toolJobRunner = new CompleteToolJobRunner();
		toolJobRunner.setConf(configuration);
		int exitCode = ToolRunner.run(toolJobRunner, args);
		System.exit(exitCode);
	}

	public static void local(String[] args) throws Exception {
		CompleteToolJobRunner toolJobRunner = new CompleteToolJobRunner();
		toolJobRunner.setConf(HadoopUtils.buildLocalConfiguration());
		int exitCode = ToolRunner.run(toolJobRunner, args);
		System.exit(exitCode);
	}
}
