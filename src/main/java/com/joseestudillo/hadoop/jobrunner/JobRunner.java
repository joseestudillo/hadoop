package com.joseestudillo.hadoop.jobrunner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.log4j.Logger;

import com.joseestudillo.hadoop.map.TokenizerMapper;
import com.joseestudillo.hadoop.reduce.IntSumReducer;
import com.joseestudillo.hadoop.util.HadoopUtils;

/**
 * WordCount for Yarn. Based on WordCount example provided by Hadoop
 * 
 * Example of how to run a MapReduce task as it was done in hadoop 1, this is also compatible with hadoop 2
 * 
 * If no paths are given, it will grab the input from /tmp/input and generate the output in /tmp/`timestamp`
 * 
 * @author Jose Estudillo
 */
public class JobRunner {

	private static final Logger log = Logger.getLogger(JobRunner.class);

	public static void main(String[] args) throws Exception {
		log.info("Creating configuration");
		Configuration configuration = HadoopUtils.buildLocalConfiguration();
		main(configuration, args);
	}

	public static void main(Configuration configuration, String[] args) throws Exception {
		String inputPath = (args.length > 1) ? args[0] : "/tmp/input/";
		String outputPath = (args.length > 2) ? args[1] : "/tmp/" + System.currentTimeMillis(); //the output path must be always new, this is this way to prevent data loss

		String username = configuration.get("hadoop.job.ugi");
		if (username != null) {
			System.setProperty("HADOOP_USER_NAME", username);
		}
		//dfs.web.ugi, user for the webinterface is by default: webuser,webgroup

		log.info("Creating the job");
		Job job = Job.getInstance(configuration, "word count");

		job.setJarByClass(JobRunner.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setReducerClass(IntSumReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		log.info("Setting input/output path");
		FileInputFormat.addInputPath(job, new Path(inputPath));
		FileOutputFormat.setOutputPath(job, new Path(outputPath));

		log.info("Waiting for completion");
		job.submit();
		int exitCode = job.waitForCompletion(true) ? 0 : 1;
		log.info("Done");
		System.exit(exitCode);

	}
}