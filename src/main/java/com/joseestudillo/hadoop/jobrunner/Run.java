package com.joseestudillo.hadoop.jobrunner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.log4j.Logger;

import com.joseestudillo.hadoop.util.HadoopUtils;

public class Run {

	private static final Logger log = Logger.getLogger(Run.class);

	public static void main(String[] args) throws Exception {

		String inputPath = (args.length > 0) ? args[0] : "";
		String outputPath = (args.length > 1) ? args[1] : "";
		String configPath = (args.length > 2) ? args[2] : "";

		Configuration configuration = HadoopUtils.loadConfig(configPath, log);
		FileSystem fs = FileSystem.get(configuration);

		log.info(String.format("Displaying configuration parameters", configPath));
		HadoopUtils.showConfig(configuration, System.out);

		if (inputPath.length() == 0 || outputPath.length() == 0) {
			String[] paths = HadoopUtils.setUpDummyInputOutput(fs, log);
			inputPath = paths[0];
			outputPath = paths[1];
		}

		log.info(String.format("input: %s, output: %s", inputPath, outputPath));

		CompleteToolJobRunner.main(configuration, new String[] { inputPath, outputPath });

	}
}
