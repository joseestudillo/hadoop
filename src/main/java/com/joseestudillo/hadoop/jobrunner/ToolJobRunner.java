package com.joseestudillo.hadoop.jobrunner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.joseestudillo.hadoop.mapreduce.CombinerImpl;
import com.joseestudillo.hadoop.mapreduce.MapperImpl;
import com.joseestudillo.hadoop.mapreduce.PartitionerImpl;
import com.joseestudillo.hadoop.mapreduce.ReducerImpl;
import com.joseestudillo.hadoop.util.HadoopUtils;

/**
 * 
 * @author Jose Estudillo
 *
 */
public class ToolJobRunner extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		if (args.length != 2) {
			System.err.printf("Usage: %s [generic options] <input> <output>\n", this.getClass().getSimpleName());
			ToolRunner.printGenericCommandUsage(System.err);
			return -1;
		}
		Configuration configuration = this.getConf();
		String username = configuration.get("hadoop.job.ugi");
		System.setProperty("HADOOP_USER_NAME", username != null ? username : "");
		Job job = Job.getInstance(configuration, "Word Count");
		job.setJarByClass(this.getClass());

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setMapperClass(MapperImpl.class);
		//combiner added for clarity, usually the reducer will be enough
		job.setCombinerClass(CombinerImpl.class);
		job.setReducerClass(ReducerImpl.class);
		job.setPartitionerClass(PartitionerImpl.class);

		//job.setInputFormatClass(TextInputFormat.class); set automatically when the input is set
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		String propertiesFilePath = "";
		if (args.length < 2) {
			System.err.println("");
			System.exit(-1);
		} else {
			if (args.length != 2) {
				propertiesFilePath = args[0];
				args = new String[] { args[1], args[2] };
			}
		}

		ToolJobRunner toolJobRunner = new ToolJobRunner();
		Configuration conf = (propertiesFilePath.length() > 0)
				? HadoopUtils.loadConfigFromProperties(propertiesFilePath)
				: HadoopUtils.buildLocalConfiguration();
		toolJobRunner.setConf(conf);
		int exitCode = ToolRunner.run(toolJobRunner, args);
		System.exit(exitCode);
	}
}
