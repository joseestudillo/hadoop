package com.joseestudillo.hadoop.jobrunner;

import com.joseestudillo.hadoop.util.HadoopUtils;

public class RunLocal {

	public static void main(String[] args) throws Exception {
		Run.main(new String[] { "", "", HadoopUtils.LOCAL_CONFIG });
	}
}
