package com.joseestudillo.hadoop.mapreduce;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.log4j.Logger;

/**
 * Copy of the default HashParitioner just for logging purposes.
 * 
 * It requires more than 1 map reduce task to be useful. Note that when running under the local job runner, only zero or one reducers are supported
 * 
 * Decides to which reduce task an split is going to
 * 
 * @author Jose Estudillo
 *
 */
public class PartitionerImpl extends Partitioner<Text, IntWritable> {

	private static final Logger log = Logger.getLogger(PartitionerImpl.class);

	@Override
	public int getPartition(Text key, IntWritable value, int numReduceTasks) {
		int partition = (key.hashCode() & Integer.MAX_VALUE) % numReduceTasks;
		log.info(String.format("Generating partition {key: %s, value:%s, num_reduce_tasks: %s, partition: %s}", key, value, numReduceTasks, partition));
		return partition;
	}

}
