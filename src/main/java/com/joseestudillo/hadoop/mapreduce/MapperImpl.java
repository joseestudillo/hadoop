package com.joseestudillo.hadoop.mapreduce;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

/**
 * 
 * @author Jose Estudillo
 *
 */
public class MapperImpl extends Mapper<LongWritable, Text, Text, IntWritable> {
	//<Input Key Type, Input Value Type, Output Value Key, Output Value Type>

	public static final Logger log = Logger.getLogger(MapperImpl.class);

	public enum CounterNames {
		MAP_CALLS;
	}

	{
		log.info("Created");
	}

	@Override
	protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context) throws IOException, InterruptedException {
		/*
		 * //values obtained from the original key and the original value String newKey = ""; int newValue = 0;
		 * 
		 * //new values generated context.write(new Text(newKey), new IntWritable(newValue));
		 */

		//custom counter creation
		context.getCounter(CounterNames.MAP_CALLS).increment(1);

		log.info(String.format("map(key=%s, value=%s)", key, value));
		Text word = new Text();
		IntWritable one = new IntWritable(1); // this should be a constant
		StringTokenizer itr = new StringTokenizer(value.toString());
		while (itr.hasMoreTokens()) {
			word.set(itr.nextToken());
			context.write(word, one);
		}
		log.info(String.format("map DONE", key, value, context));
	}

}
