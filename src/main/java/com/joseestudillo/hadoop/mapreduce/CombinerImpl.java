package com.joseestudillo.hadoop.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.Logger;

/**
 * The combiner do a reduction at node level (local reduction), so it can be useful to reduce the amount on information send over the network
 *
 * The implementation is the same as the reducer
 * 
 * @author Jose Estudillo
 * 
 */
public class CombinerImpl extends Reducer<Text, IntWritable, Text, IntWritable> {

	public static final Logger log = Logger.getLogger(CombinerImpl.class);

	{
		log.info("Created");
	}

	@Override
	protected void reduce(Text key, Iterable<IntWritable> valuesAssociatedToTheKey, Reducer<Text, IntWritable, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {

		List<Integer> valuesToLog = new ArrayList<Integer>();
		int sum = 0;
		for (IntWritable val : valuesAssociatedToTheKey) {
			sum += val.get();
			valuesToLog.add(val.get());
		}

		log.info(String.format("(key=%s, values=%s) reduced to (key=%s, values=%s)", key, valuesToLog, key, sum));
		context.write(key, new IntWritable(sum));

		log.info("reduce DONE");
	}
}