package com.joseestudillo.hadoop.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.Logger;

/**
 * 
 * @author Jose Estudillo
 *
 */
public class ReducerImpl extends Reducer<Text, IntWritable, Text, IntWritable> {

	public static final Logger log = Logger.getLogger(ReducerImpl.class);

	{
		log.info("Created");
	}

	@Override
	protected void reduce(Text key, Iterable<IntWritable> valuesAssociatedToTheKey, Reducer<Text, IntWritable, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {
		/*
		 * //do the reduction of the values add the result value to the context Iterable<IntWritable> => IntWritable IntWritable resultValue = new
		 * IntWritable(0); context.write(key, resultValue);
		 * 
		 * the iterable can only be iterated once
		 */

		List<Integer> valuesToLog = new ArrayList<Integer>();
		int sum = 0;
		for (IntWritable val : valuesAssociatedToTheKey) {
			sum += val.get();
			valuesToLog.add(val.get());
		}

		log.info(String.format("(key=%s, values=%s) reduced to (key=%s, values=%s)", key, valuesToLog, key, sum));
		context.write(key, new IntWritable(sum));

		log.info("reduce DONE");
	}
}
