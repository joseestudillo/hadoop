package com.joseestudillo.hadoop.comparator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.log4j.Logger;

/**
 * Comparator using during the mapping
 * 
 * @author Jose Estudillo
 *
 */
public class KeyComparator extends WritableComparator {

	private static final Logger log = Logger.getLogger(KeyComparator.class);

	public KeyComparator() {
		super(Text.class, true); //without this initialization it doesnt work properly, It must know which types is comparing
	}

	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		int result = ((Text) a).compareTo((Text) b);//super.compare(a, b);
		log.info(String.format("compare(%s, %s) = %s", a, b, result));
		return result;
	}

}
