package com.joseestudillo.hadoop.comparator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.log4j.Logger;

/**
 * Comparator used during the reduction
 * 
 * @author Jose Estudillo
 *
 */

public class GroupComparator extends WritableComparator {

	private static final Logger log = Logger.getLogger(GroupComparator.class);

	public GroupComparator() {
		// without the following initialization it doesn't work properly, It must
		// know which types is comparing
		super(Text.class, true);
	}

	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		int result = super.compare(a, b);
		log.info(String.format("compare(%s, %s) = %s", a, b, result));
		return result;
	}

}
