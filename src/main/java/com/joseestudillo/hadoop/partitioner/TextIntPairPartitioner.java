package com.joseestudillo.hadoop.partitioner;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.log4j.Logger;

/**
 * The partitioner allows you to decide to with partition you want to send a pair. In this case we will put together the ones with the same key.
 * 
 * @author Jose Estudillo
 * 
 */
public class TextIntPairPartitioner extends Partitioner<Text, IntWritable> {

	private static final Logger log = Logger.getLogger(TextIntPairPartitioner.class);

	@Override
	public int getPartition(Text key, IntWritable value, int numPartitions) {
		int result = key.hashCode() % numPartitions;
		log.info(String.format("Pair(%s, %s) assigned to partition %s #partitions: 2", key, value, result, numPartitions));
		return result;
	}
}