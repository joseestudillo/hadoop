package com.joseestudillo.avro;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;

/**
 * Utility class for Avro
 * 
 * @author Jose Estudillo
 *
 */
public class AvroUtils {

	public static Schema readFromFile(File avscFile) throws IOException {
		return new Schema.Parser().parse(avscFile);
	}

	public static <T> void serialize(File outputFile, Schema schema, List<T> toSerialize) throws IOException {
		DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(schema);
		DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(datumWriter);
		dataFileWriter.create(schema, outputFile);
		for (T item : toSerialize) {
			dataFileWriter.append((GenericRecord) item);
		}
		dataFileWriter.close();
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> deserialize(File avroFile, Schema schema) throws IOException {
		DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
		DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(avroFile, datumReader);
		GenericRecord record = null;
		List<T> deserializedItems = new ArrayList<T>();
		while (dataFileReader.hasNext()) {
			record = dataFileReader.next(record);
			deserializedItems.add((T) record);
		}
		dataFileReader.close();
		return deserializedItems;
	}
}
