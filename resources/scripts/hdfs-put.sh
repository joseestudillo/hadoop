source ./vars.sh

CMD="hdfs dfs \
-conf $CONFIG_FILE \
-put $1 /tmp/"

# alternative way to run the command line without the need of a configuration file
#-Dfs.defaultFS=hdfs://hostname:8020 \
#-Ddfs.datanode.address=hostname:50010 \ 
#-Ddfs.client.use.datanode.hostname=true

echo $CMD
eval $CMD
