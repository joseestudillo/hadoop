JAR=target/hadoop-0.0.1-SNAPSHOT.jar 
CLASS=com.joseestudillo.hadoop.util.KerberosTester 
#PRINCIPAL=hdfs-Sandbox@HORTONWORKS.COM
#PRINCIPAL=nn/sandbox.hortonworks.com@HORTONWORKS.COM
PRINCIPAL=root/admin@HORTONWORKS.COM

KEYTAB=/tmp/full-keytab.keytab
HADOOP_CONF_DIR=./resources/config-hortonworks

#export HADOOP_OPTS=" -Djava.security.krb5.realm=HORTONWORKS.COM -Djava.security.krb5.kdc=sandbox.hortonworks.com -Djava.security.krb5.conf=/etc/krb5.conf"
#export JAVA_HOME=`/usr/libexec/java_home -v 1.6`
#echo $JAVA_HOME
CMD="java -Dlog4j.configuration=file://`pwd`/src/main/resources/log4j.xml -cp $JAR $CLASS $PRINCIPAL $KEYTAB $HADOOP_CONF_DIR"
echo "$CMD"
eval $CMD


