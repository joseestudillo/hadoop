#Hadoop

# Concepts

[TODO]

# Running Hadoop remotely

There are two ways to run hadoop remotely:

- Creating a jar file that contains all the require dependencies and run it into the hadoop host using `hadoop jar JAR_FILE.jar`. This command allow to add extra properties that can be passed using `-D`, don't confuse setting Hadoop properties using the `-D property=value` option to GenericOptionsParser (and ToolRunner) with setting JVM system properties using the `-Dproperty=value` option to the java command

- Loading the configuration into hadoop manually and executing the application remotely:

```
Configuration conf = ...
conf.addResource(...);
```

In the class `HadoopUtils` I have implemented a few methods that allow to load and generate configurations. Notice that this project intents to help learning hadoop, so many of the methods are not designed to be used in production (neither have the quality to do so).

The hadoop configuration files are usually called `core-site.xml, hdfs-site.xml, mapred-site.xml, yarn-site.xml`, and are stored into `/etc/hadoop`.

For this kind of configuration is very important to make the hostnames visible in the host machine and in the Virtual Machine. 

## Configuring The project to run jobs remotely in the Cloudera Quick Start VM

The first thing required will be obtain the configuration as it is defined in the cluster, for this example we will assume that the host can access to the cluster through the hostname `quickstart.cloudera` (this hostname can also be resolved by the cloudera vm).

To do so:
```bash
rsync -rviP root@quickstart.cloudera:/etc/hadoop/conf PROJECT_FOLDER/src/main/resources/config-cloudera
```

Notice that these files are already included in the project (version 5.4) with some modifications require to make it work remotely, that I will describe below.

- Set the right hostname for all the services replacing `0.0.0.0` with `quickstart.cloudera`:

```bash
find . -name "*.xml" | xargs sed -i "s/0.0.0.0/quickstart.cloudera/g" #this only works with GNU sed (not included in osx by default)
```

- define YARN host explicitly in the file `yarn-site.xml` adding the properties:

```xml
  ...
  <property>
    <name>yarn.resourcemanager.resource-tracker.address</name>
    <value>quickstart.cloudera:8031</value>
  </property>
  <property>
    <name>yarn.resourcemanager.scheduler.address</name>
    <value>quickstart.cloudera:8030</value>
  </property>
  <property>
    <name>yarn.resourcemanager.address</name>
    <value>quickstart.cloudera:8032</value>
  </property>
  ...
```

- finally, you need to force hadoop to use the hostnames for the datanodes setting the following in `hdfs-site.xml`:

```xml
  <property>
    <name>dfs.client.use.datanode.hostname</name>
    <value>true</value>
  </property>
```

- To run a quick example, go to the project folder and use on the CLI (this can't be launched from the IDE because it required the jar generated):

```bash
mvn package
./run-cloudera-jar.sh
```

This script will launch the class `com.joseestudillo.hadoop.jobrunner.Run` with the configuration stored in `PROJECT_FOLDER/resources/config-cloudera` That generates a dummy input and run word count over it. If you want to run it with a different input specify `inputPath outputPah [configPath]` notice that configPath is only required if you are submitting the hadoop job remotely or running it locally.
 

## Hadoop CLI

The hadoop HDFS client provides many of the commands available in the linux CLI, as an example

```bash
hdfs dfs -mkdir -p hdfs://cluster-vm/tmp/test
hdfs dfs -chmod -R o+rw hdfs://cluster-vm/tmp/test
hdfs dfs -put file.txt hdfs://cdh-vm/tmp

# Code Examples

- `com.joseestudillo.hadoop.util.KerberosTest`: Utility to test the access to a kerberized instance.